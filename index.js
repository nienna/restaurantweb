
function getFileFromServer(url, doneCallback) {
    let xhr;
    xhr = new XMLHttpRequest();
    xhr.onreadystatechange = handleStateChange;
    xhr.open("GET", url, true);
    xhr.send();

    function handleStateChange() {
        if (xhr.readyState === 4) {
            doneCallback(xhr.status == 200 ? xhr.responseText : null);
        }
    }
}

function cleanlayers(){
    console.log("Clean");
    i = 0
    map.eachLayer(Layer => {
        if(i>1){
            console.log("cleanLayer");
            map.removeLayer(Layer);
        }
        ++i;
    })
}

function showRestaurants(){
    cleanlayers();
    let nameValue = document.getElementById("restaurant-form").value;
    let markers = L.markerClusterGroup({maxClusterRadius: 15});
    getFileFromServer("data.txt", function(text) { //Very bad read the file every change, temporal until add DB
        if (text === null) {
            console.log("Error");
        }
        else {
            allTextLines = text.split(/\r\n|\n/);
            for (index = 0; index < allTextLines.length-1; index++) { 
                markerinfo = allTextLines[index].split(";");
                if(markerinfo[6] == nameValue){
                    let icono = L.icon({ //8
                        iconUrl: 'icons/marker-'+markerinfo[6]+'.png',
                    
                        iconSize:     [25, 41], // size of the icon
                        iconAnchor:   [12, 41], // point of the icon which will correspond to marker's location
                        popupAnchor:  [1, -33] // point from which the popup should open relative to the iconAnchor
                    });
                    markers.addLayer(L.marker([markerinfo[0],markerinfo[1]], {icon: icono}).addTo(map).bindPopup("<b>"+markerinfo[2]+"</b><br>"+markerinfo[3]+"<br><a href=\"https://maps.google.com/?q="+markerinfo[4]+"\">"+markerinfo[4]+"</a><br><br>"+markerinfo[5]));
                }
            } 
        }
    });

    map.addLayer(markers);
}


let map = L.map('map').setView([41.3825, 2.176944],13);
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
maxZoom: 18}).addTo(map);
L.control.scale().addTo(map);



